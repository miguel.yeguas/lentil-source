# Lentil Source

For compiling the library navigate to the pota folder and executes ./scripts/init.cmd and ./scripts/build.cmd

Currently the ARNOLD_ROOT and ARNOLD_VERSION is set in the pota/CMakePresets.json for both windows and linux (wsl) compilation

Currently only the .dll/.os and .mtd are being generated under pota/dist/. The distrubution folder is generated automatically from the INSTALL_ROOT variable set in the pota/CMakeLists.txt